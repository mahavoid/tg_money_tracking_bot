import itertools
from typing import List

currencies = {
    'RUB': ['р', 'руб', 'руб.', 'рублей', 'rub'],
    'AMD': ['д', 'драм', 'драмм', 'драмов', 'amd']
}
currency_prefixes: List[str] = list(itertools.chain.from_iterable([v for v in currencies.values()]))
