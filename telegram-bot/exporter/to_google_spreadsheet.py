import logging
from datetime import datetime
from typing import List

import gspread
from gspread.utils import ValueInputOption
from oauth2client.service_account import ServiceAccountCredentials

from models import MoneyTrackingItem


class ToGoogleSpreadsheetExporter():

    def create_clean_sheet(self, date: datetime):
        old_sheets = self.workbook.worksheets()
        sheet = self.workbook.duplicate_sheet(old_sheets[0].id,
                                              new_sheet_id=old_sheets[-1].id + 1,
                                              new_sheet_name=date.strftime('%m-%Y'))
        rowcount = sheet.row_count
        sheet.batch_clear([f'A2:F{rowcount}'])
        self.workbook.reorder_worksheets([*old_sheets, sheet])
        return sheet

    def __init__(self, spreadsheet_url, json_keyfile_name):
        scope = ["https://spreadsheets.google.com/feeds",
                 'https://www.googleapis.com/auth/spreadsheets',
                 "https://www.googleapis.com/auth/drive.file",
                 "https://www.googleapis.com/auth/drive"]

        creds = ServiceAccountCredentials.from_json_keyfile_name(json_keyfile_name, scope)
        self.client = gspread.authorize(creds)
        self.spreadsheet_url = spreadsheet_url

        self.workbook = self.client.open_by_url(self.spreadsheet_url)
        self.worksheet = None
        self.current_date = None

        today = datetime.today()

        self.update_date_and_worksheet(today)

    @staticmethod
    def __get_to_rub_formula(index: int) -> str:
        return f'=IF(C{index}="AMD"; B{index}*index(GOOGLEFINANCE("CURRENCY:AMDRUB"; "price"; A{index}; 0); 2; 2); B{index})'

    def next_available_row(self) -> int:
        str_list = list(filter(None, self.worksheet.col_values(1)))
        return len(str_list) + 1

    def add_track_rows_to_worksheet(self, items: List[MoneyTrackingItem]):
        # for item in items:
        available_row_index = self.next_available_row()
        try:
            self.worksheet.batch_update([{
                'range': f'A{available_row_index}:F{available_row_index + (len(items) - 1)}',
                'values': [[self.current_date.strftime('%d.%m.%Y'),
                            item.amount,
                            item.currency,
                            item.target,
                            item.item_of_expenditure,
                            ToGoogleSpreadsheetExporter.__get_to_rub_formula(available_row_index + idx)]
                           for idx, item in enumerate(items)]
            }],
                value_input_option=ValueInputOption.user_entered)

        except:
            self.worksheet.batch_clear([f'A{available_row_index}:F{available_row_index + (len(items) - 1)}'])
            raise

    async def export(self, items: List[MoneyTrackingItem], reply_callback):
        today = datetime.today()
        if self.current_date != today:
            self.update_date_and_worksheet(today)

        try:
            self.add_track_rows_to_worksheet(items)
        except:
            logger = logging.getLogger()
            logger.error(f'Ошибка записи в спредшит. Строки для записи: {str(items)}', exc_info=True)
            await reply_callback('Не смог добавить в записи из-за ошибки :с')
            return

        await reply_callback('Добавил в записи: \n' + '\n'.join([str(x) for x in items]))

    def update_date_and_worksheet(self, today: datetime):
        self.current_date = today
        worksheets = self.workbook.worksheets()
        worksheets = [ws for ws in worksheets if ws.title == today.strftime('%m-%Y')]
        self.worksheet = worksheets[0] if len(worksheets) > 0 else self.create_clean_sheet(date=today)
