from typing import List

from dictionaries import currency_prefixes, currencies, expenditures_by_target
from models import MoneyTrackingItem


def try_parse_amount_and_currency(s: str):
    currency = 'AMD'
    prefixes = [x for x in currency_prefixes if s.lower().endswith(x.lower())]
    if len(prefixes) > 0:
        s = s[:-len(prefixes[0])]
        currency = [key for key in currencies.keys() if prefixes[0].lower() in currencies[key]][0]

    try:
        float(s)
        return float(s), currency
    except:
        return None, None


def try_parse_item_of_expenditure(target: str) -> str:
    for key in expenditures_by_target.keys():
        if target.lower() in expenditures_by_target[key]:
            return key

    return 'Не указано'


class Parser:
    @staticmethod
    def text_to_money_tracking_items(text: str) -> List[MoneyTrackingItem]:
        if text is None or len(text) == 0:
            return []

        strings = text.split('\n')
        if len(strings) == 0:
            return []

        strings_to_parse = len(strings)

        results = []
        for string in strings:
            if string.startswith('- '):
                strings_to_parse -= 1
                continue

            parts = string.split(' ')
            parts = [part.strip() for part in parts if part.strip() != '']

            if len(parts) < 2:
                continue

            (amount, currency) = try_parse_amount_and_currency(parts[0])

            if None is [amount, currency]:
                continue

            if parts[1].lower() in currency_prefixes and len(parts) > 2:
                currency = [key for key in currencies.keys() if parts[1].lower() in currencies[key]][0]
                target = ' '.join(parts[2:])
            else:
                target = ' '.join(parts[1:])

            if None in [amount, currency, target]:
                continue

            item_of_expenditure = try_parse_item_of_expenditure(target)

            mti = MoneyTrackingItem(amount=amount, currency=currency, target=target,
                                    item_of_expenditure=item_of_expenditure)
            results.append(mti)

        if len(results) != strings_to_parse:
            return []

        return results
