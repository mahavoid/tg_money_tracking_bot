from functools import lru_cache

from pydantic import Extra
from pydantic_settings import BaseSettings, SettingsConfigDict


class Settings(BaseSettings):
    TELEGRAM_BOT_TOKEN: str = None
    SPREADSHEET_URL: str

    model_config = SettingsConfigDict(env_file=('.env.prod', '.env.test'), extra=Extra.ignore)


@lru_cache
def get_settings():
    return Settings()
