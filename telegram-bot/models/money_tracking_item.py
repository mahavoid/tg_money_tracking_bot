from pydantic import BaseModel
from transformers import pipeline, BartForSequenceClassification, BartTokenizer

import logging

class MoneyTrackingItem(BaseModel):
    amount: float
    currency: str = 'AMD'
    target: str
    item_of_expenditure: str

    def __str__(self):
        return f'{self.amount} {self.currency} на {self.target}, категория: {self.item_of_expenditure}'

    def __repr__(self):
        return self.__str__()


class PurchaseClassifier:
    def __init__(self, model_name="facebook/bart-large-mnli"):
        self.tokenizer = BartTokenizer.from_pretrained(model_name)
        self.model = BartForSequenceClassification.from_pretrained(model_name)
        self.classifier = pipeline("zero-shot-classification", model=self.model, tokenizer=self.tokenizer)
        self.categories = [
            "техника", "одежда", "еда", "транспорт", "развлечение", 
            "бытовая химия", "книги", "медицина", "спорт", "образование", 
            "дом и сад", "косметика", "игрушки", "музыка", "туризм"
        ]
        
    def classify(self, text):
        """
        Классифицирует текст и возвращает три лучших категории с их оценками.
        
        Аргументы:
        - text (str): Входной текст.
        
        Возвращает:
        - list: Топ 3 категории с их оценками.
        """
        try:
            result = self.classifier(text, self.categories, multi_label=False)
            top_3_labels = result['labels'][:3]
            top_3_scores = result['scores'][:3]
            return list(zip(top_3_labels, top_3_scores))
        except Exception as e:
            logging.error(f"Ошибка при классификации текста: {e}")
            return [("Ошибка при классификации", 0)]
