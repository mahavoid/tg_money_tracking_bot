import logging
import os
import warnings
from logging.handlers import TimedRotatingFileHandler
from typing import List

from telegram import Update
from telegram.constants import ParseMode
from telegram.ext import ApplicationBuilder, ContextTypes, CommandHandler, MessageHandler, filters

from exporter import ToGoogleSpreadsheetExporter
from help_info import help_strings
from models.money_tracking_item import MoneyTrackingItem
from parser import Parser
from settings.settings import get_settings


async def start(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id, text="I'm a bot, please talk to me!")


async def _help(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await context.bot.send_message(chat_id=update.effective_chat.id,
                                   text='\n'.join(help_strings),
                                   parse_mode=ParseMode.MARKDOWN_V2)


def reply_to_message(update: Update, context: ContextTypes.DEFAULT_TYPE):
    async def _callback(text: str):
        await context.bot.send_message(chat_id=update.effective_chat.id,
                                       reply_to_message_id=update.message.id,
                                       text=text)

    return _callback


def get_track_money_message_func(_exporter: ToGoogleSpreadsheetExporter):
    async def __track_money_message(update: Update, context: ContextTypes.DEFAULT_TYPE):
        try:
            items: List[MoneyTrackingItem] = Parser.text_to_money_tracking_items(update.message.text)
        except:
            _logger = logging.getLogger()
            _logger.error(f'Ошибка парсинга. Исходное сообщение: {update.message.text}', exc_info=True)
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           reply_to_message_id=update.message.id,
                                           text='Не смог попарсить из-за ошибки :(')
            return

        if len(items) == 0:
            _logger = logging.getLogger()
            _logger.warning(f'Отпарсил 0 строк. Исходное сообщение: {update.message.text}')
            await context.bot.send_message(chat_id=update.effective_chat.id,
                                           reply_to_message_id=update.message.id,
                                           text='Не смог попарсить :(')
        else:
            await _exporter.export(items, reply_callback=reply_to_message(update, context))

    return __track_money_message


def init_logging() -> logging.Logger:
    _settings = get_settings()
    _logger = logging.getLogger()
    _logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()

    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    _logger.addHandler(handler)

    file_handler = TimedRotatingFileHandler(f"logs/log.log",
                                            when="midnight", backupCount=60)

    file_handler.suffix = "%Y%m%d"
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.DEBUG)
    _logger.addHandler(file_handler)

    warnings.filterwarnings("ignore", category=DeprecationWarning)

    for module in ['urllib3', 'telegram', 'asyncio', 'httpx', 'httpcore', 'google']:
        logging.getLogger(module).setLevel(logging.WARN)
    # logging.getLogger().setLevel(logging.WARN)

    return _logger


def create_service_dirs():
    os.makedirs('logs', exist_ok=True)


if __name__ == '__main__':
    settings = get_settings()
    create_service_dirs()
    logger = init_logging()
    logger.info('App starting')
    try:
        application = ApplicationBuilder().token(settings.TELEGRAM_BOT_TOKEN).build()

        start_handler = CommandHandler('start', start)
        application.add_handler(start_handler)

        help_handler = CommandHandler('help', _help)
        application.add_handler(help_handler)

        exporter = ToGoogleSpreadsheetExporter(settings.SPREADSHEET_URL, 'creds.json')

        money_handler = MessageHandler(filters.TEXT, get_track_money_message_func(exporter))
        application.add_handler(money_handler)

        logger.info('App started')
        application.run_polling()
    except:
        logger.critical('Ошибка верхнего уровня', exc_info=True)
        raise
