import torch
from transformers import BartTokenizer, BartForSequenceClassification, Trainer, TrainingArguments

# 1. Data Preparation
# (Assuming you have data in the format: [{"text": "some text", "label": "some category"}, ...])
data = [...]  # Load your data here
labels = list(set([item['label'] for item in data]))
label2id = {label: i for i, label in enumerate(labels)}
id2label = {i: label for label, i in label2id.items()}

# Convert labels to label IDs
for item in data:
    item['label_id'] = label2id[item['label']]

# Split data into training and validation sets
train_size = int(0.8 * len(data))
train_data = data[:train_size]
val_data = data[train_size:]

# 2. Dataset Creation
class CustomDataset(torch.utils.data.Dataset):
    def __init__(self, tokenizer, data, max_length=128):
        self.tokenizer = tokenizer
        self.data = data
        self.max_length = max_length

    def __len__(self):
        return len(self.data)

    def __getitem__(self, idx):
        item = self.data[idx]
        encoding = self.tokenizer(item['text'], truncation=True, max_length=self.max_length, padding='max_length', return_tensors='pt')
        return {
            'input_ids': encoding['input_ids'].flatten(),
            'attention_mask': encoding['attention_mask'].flatten(),
            'labels': torch.tensor(item['label_id'])
        }

# 3. Model & Tokenizer Initialization
model_name = "facebook/bart-large-mnli"
tokenizer = BartTokenizer.from_pretrained(model_name)
model = BartForSequenceClassification.from_pretrained(model_name, num_labels=len(labels))
model.config.id2label = id2label
model.config.label2id = label2id

# 4. Training Configuration
training_args = TrainingArguments(
    per_device_train_batch_size=8,
    per_device_eval_batch_size=8,
    num_train_epochs=3,
    evaluation_strategy="epoch",
    save_strategy="epoch",
    logging_dir="./logs",
    logging_steps=10,
    save_total_limit=2,
    push_to_hub=False,
)

# 5. Model Training
train_dataset = CustomDataset(tokenizer, train_data)
val_dataset = CustomDataset(tokenizer, val_data)

trainer = Trainer(
    model=model,
    args=training_args,
    train_dataset=train_dataset,
    eval_dataset=val_dataset,
)

trainer.train()

# 6. Evaluation
results = trainer.evaluate()

# 7. Save the Model
model_path = "./fine_tuned_bart"
model.save_pretrained(model_path)
tokenizer.save_pretrained(model_path)
