from flask import Flask

from purchase_classifier.purchase_classifier import PurchaseClassifier

app = Flask(__name__)

purchase_classifier = PurchaseClassifier()


@app.route("/healthcheck")
def healthcheck():
    return '', 200


@app.route("/classify-purchase/<payment>")
def classify_purchase(payment: str):
    classifications = purchase_classifier.classify(payment)
    print(classifications)
    return classifications
