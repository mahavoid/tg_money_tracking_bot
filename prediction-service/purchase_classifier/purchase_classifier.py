import logging
from typing import List, Tuple

from transformers import pipeline, BartForSequenceClassification, BartTokenizer


class PurchaseClassifier:
    def __init__(self, model_name="facebook/bart-large-mnli"):
        self.tokenizer = BartTokenizer.from_pretrained(model_name)
        self.model = BartForSequenceClassification.from_pretrained(model_name)
        self.classifier = pipeline("zero-shot-classification", model=self.model, tokenizer=self.tokenizer)
        self.categories = [
            "Алкоголь и наркотики",
            "Аренда",
            "Благотворительность",
            "Еда",
            "Еда -> Сладости",
            "Животные",
            "Коммунальные платежи",
            "Косметика",
            "Медицина",
            "Образование",
            "Одежда и обувь",
            "Подписки",
            "Рестораны и бары",
            "Транспорт",
            "Хобби",
            "Хозяйственное",
            "Терапия",
            "Дом и уют",
            "Развлечения",
            "Путешествия",
            "Друзья / Родственники",
            "Гигиена",
            "Техника",
            "Не указано"
        ]

    def classify(self, text) -> List[Tuple[str, float]]:
        """
        Классифицирует текст и возвращает три лучших категории с их оценками.

        Аргументы:
        - text (str): Входной текст.

        Возвращает:
        - list: Топ 3 категории с их оценками.
        """
        result = self.classifier(text, self.categories, multi_label=False)
        top_3_labels = result['labels'][:3]
        top_3_scores = result['scores'][:3]
        return list(zip(top_3_labels, top_3_scores))
