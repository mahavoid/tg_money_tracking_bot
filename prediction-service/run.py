import logging.config
import os
import platform
import warnings
from logging.handlers import TimedRotatingFileHandler

from app import app

if platform.uname().system.lower() == 'linux':
    import gunicorn.app.base


    class StandaloneApplication(gunicorn.app.base.BaseApplication):
        def __init__(self, _app, _options=None):
            self.options = _options or {}
            self.application = _app
            super().__init__()

        def load_config(self):
            config = {key: value for key, value in self.options.items()
                      if key in self.cfg.settings and value is not None}
            for key, value in config.items():
                self.cfg.set(key.lower(), value)

        def load(self):
            return self.application


def create_service_dirs():
    os.makedirs('logs', exist_ok=True)


def init_logging() -> logging.Logger:
    # _settings = get_settings()
    _logger = logging.getLogger()
    _logger.setLevel(logging.DEBUG)
    handler = logging.StreamHandler()

    formatter = logging.Formatter("%(asctime)s - %(name)s - %(levelname)s - %(message)s")
    handler.setFormatter(formatter)
    _logger.addHandler(handler)

    file_handler = TimedRotatingFileHandler(f"logs/log.log",
                                            when="midnight", backupCount=60)

    file_handler.suffix = "%Y%m%d"
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.DEBUG)
    _logger.addHandler(file_handler)

    # warnings.filterwarnings("ignore", category=DeprecationWarning)

    # for module in ['urllib3', 'telegram', 'asyncio', 'httpx', 'httpcore', 'google']:
    #     logging.getLogger(module).setLevel(logging.WARN)
    # logging.getLogger().setLevel(logging.WARN)

    return _logger


def main():
    create_service_dirs()
    logger = init_logging()
    logger.info('App starting')
    if platform.uname().system.lower() == 'linux':
        options = {
            'bind': '%s:%s' % ('0.0.0.0', '80'),
            'timeout': 30,
            'max-requests': 100,
            'max-requests-jitter': 5,
            'workers': 4
        }
        StandaloneApplication(app, options).run()
    else:
        app.run(debug=True, host='0.0.0.0', port=80)


if __name__ == "__main__":
    main()
